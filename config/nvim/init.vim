runtime! config/general.vim
runtime! config/keybindings.vim
runtime! config/plugged.vim
runtime! config/plugins/*.vim
