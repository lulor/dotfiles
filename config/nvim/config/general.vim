" =============================
" ========== GENERAL ==========
" =============================

syntax on
set number
set relativenumber
set cursorline
set autoindent
filetype plugin indent on
set hidden
set lazyredraw
set mouse+=a

" --- Tabs/Spaces ---
set expandtab   
set shiftwidth=8
set softtabstop=8
" set tabstop=8

" --- Folding ---
set foldmethod=syntax
set foldlevel=99

" --- Indent Guides ---
" set listchars=tab:\|\ 
" autocmd FileType sh,c,cpp,make,python,java,go,xml,css,javascript,json set list

" --- System Clipboard ---
set clipboard=unnamed

" --- Italic for Terminal.app ---
" let &t_ZH="\e[3m"
" let &t_ZR="\e[23m"

" set showtabline=2    "Always show the Tabline

" ========== STATUSLINE ==========

" set statusline=\ %F
" set statusline+=\ %r%m
" set statusline+=%=
" set statusline+=%y
" set statusline+=%8p%%
" set statusline+=%8l,%c\ 
