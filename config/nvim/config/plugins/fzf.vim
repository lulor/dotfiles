" ========== Fzf ==========

set rtp+=/usr/local/opt/fzf

nnoremap <silent> - :Files<CR>
nnoremap <silent> _ :Files ~<CR>
nnoremap <silent> + :Buffers<CR>
nnoremap <silent> ò :Buffers<CR>
nnoremap <silent> ; :Commands<CR>
nnoremap <silent> à :Commands<CR>
