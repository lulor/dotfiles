" ========== Markdown Preview ==========

nnoremap <silent> <leader>p :MarkdownPreview<CR>
let g:mkdp_page_title = '${name}'
let g:mkdp_auto_close = 0
