" ========== Lightline ==========

set noshowmode
let g:lightline = {
			\ 'colorscheme': 'srcery',
			\ 'active': {
			\   'left': [ [ 'mode', 'paste' ],
			\             [ 'readonly', 'filename', 'modified', 'gitstatus', 'cocstatus' ] ]
			\ },
			\ 'component_function': {
			\   'gitstatus': 'GitStatus',
			\   'cocstatus': 'coc#status'
			\ },
			\ }

function! GitStatus() abort
	let status = get(g:, 'coc_git_status', '')
	" let status = FugitiveStatusline()
	return winwidth(0) >= 80 ? status : ''
endfunction
